package com.amss.amssMaster;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.amss.logs.MessageLog;
import com.amss.submethods.Paybill;
import com.amss.utils.BaseTest;

import Library.Utility;

public class AMSS extends BaseTest {

	private ArrayList<String> methodTestStatus;
	private String filename = null;
	Paybill paybill = new Paybill();
	Actions actions = new Actions(webDriver);
	public void HomePage() throws InterruptedException{
		count++;
		methodTestStatus = new ArrayList<String>();
		String methodName = "HomePage()";
		MessageLog msgLog;
		msgLog = new MessageLog(getDebugClassName(), methodName, "Entered");
		writeLogMessageInfo(msgLog);
		methodTestStatus.add(methodName);
		
		try{
			webDriver.get(LaunchUrl);
			setValueById("clfy29","ClarifyCRM Customer Interaction Manager");
			setValueById("_username",propertyMap.get("userName"));
			setValueById("_passowrd",propertyMap.get("password"));
			setValueById("clfy59","EST - Eastern Standard Time");
			setValueById("password",propertyMap.get("password"));
			
			methodTestStatus.add("Success");
			methodTestStatus.add(" ");
		}catch(Exception ex){
			ex.printStackTrace();
			msgLog = new MessageLog(getDebugClassName(), methodName);
			writeLogMessageError(msgLog,ex);
			methodTestStatus.add("Failure");
			methodTestStatus.add(getStackTrace(ex));
		}finally{
			msgLog = new MessageLog(getDebugClassName(), methodName, "Exit");
			writeLogMessageInfo(msgLog);
			getFinalReport().put(getDebugClassName()+"-"+String.valueOf(count), methodTestStatus);
		}
	}

	
	public void runAmss()  throws InterruptedException{
		count++;
		methodTestStatus = new ArrayList<String>();
		String methodName = "AMSS Login ";
		MessageLog msgLog;
		msgLog = new MessageLog(getDebugClassName(), methodName, "Entered");
		writeLogMessageInfo(msgLog);
		methodTestStatus.add(methodName);
		try{
	    webDriver.get(LaunchUrl);
		/*setValueByName("u", propertyMap.get("userName"));
		setValueByName("p", propertyMap.get("password"));
		Utility.takeScreenShots(webDriver, "LoginAmss");
		WebElement mySelectElement = webDriver.findElement(By.id("env"));
		Select dropdown= new Select(mySelectElement);
		dropdown.selectByVisibleText("A");
		webDriver.findElement(By.className("submitbutton")).click();
		Utility.takeScreenShots(webDriver, "Authentication");*/
		setValueById("homeLoginFormOptimumId", propertyMap.get("OptUsername"));
		setValueById("homeLoginFormPassword", propertyMap.get("Optpassward"));
		Utility.takeScreenShots(webDriver, "Optimumlogin");
		webDriver.findElement(By.xpath("//*[@id='homeLoginForm']/div/div[8]/div/button")).click();
		methodTestStatus.add("Success");
		methodTestStatus.add(" ");
		}catch(Exception ex){
			ex.printStackTrace();
			msgLog = new MessageLog(getDebugClassName(), methodName);
			writeLogMessageError(msgLog,ex);
			methodTestStatus.add("Failure");
			methodTestStatus.add(getStackTrace(ex));
			
		}finally{
			msgLog = new MessageLog(getDebugClassName(), methodName, "Exit");
			writeLogMessageInfo(msgLog);
			getFinalReport().put(getDebugClassName()+"-"+String.valueOf(count), methodTestStatus);
		}
		
	}
	public void payBill() throws InterruptedException{
		count++;
		methodTestStatus = new ArrayList<String>();
		String methodName = "Pay Bill";
		MessageLog msgLog;
		msgLog = new MessageLog(getDebugClassName(), methodName, "Entered");
		writeLogMessageInfo(msgLog);
		methodTestStatus.add(methodName);
		try{
		webDriver.findElement(By.xpath("//*[@id='headerNotShown']/div[2]/div/div/div/div[2]/div/a")).click();
		paybill.viewMyBill(webDriver,actions);
		paybill.myBillExplainred(webDriver);
		methodTestStatus.add("Success");
		methodTestStatus.add(" ");
		}catch(Exception ex){
			ex.printStackTrace();
			msgLog = new MessageLog(getDebugClassName(), methodName);
			writeLogMessageError(msgLog,ex);
			methodTestStatus.add("Failure");
			methodTestStatus.add(getStackTrace(ex));
		}finally{
			msgLog = new MessageLog(getDebugClassName(), methodName, "Exit");
			writeLogMessageInfo(msgLog);
			getFinalReport().put(getDebugClassName()+"-"+String.valueOf(count), methodTestStatus);
		}
		
	}
	public void manageAutomaticPayment(){
		count++;
		methodTestStatus = new ArrayList<String>();
		String methodName = "Manage Automatic Payment";
		MessageLog msgLog;
		msgLog = new MessageLog(getDebugClassName(), methodName, "Entered");
		writeLogMessageInfo(msgLog);
		methodTestStatus.add(methodName);
		try{
		
		actions.moveToElement(webDriver.findElement(By.xpath("//*[@id='headerNotShown']/div[2]/div/div/div/div[2]/div/a"))).click(webDriver.findElement(By.xpath("//*[@id='headerNotShown']/div[2]/div/div/div/div[2]/div/div/div[2]/div/div[1]/ul[5]/li[2]/a"))).build().perform();
		/*clickElementByXPath("//*[@id='enrolled-content']/div/div[2]/div[2]/div[1]/div[2]/div[2]/div[2]/span/a");*/
		//webDriver.findElement(By.xpath("//*[@id='enrolled-content']/div/div[2]/div[2]/div[1]/div[2]/div[2]/div[2]/span/a")).click();
		methodTestStatus.add("Success");
		methodTestStatus.add(" ");
		}catch(Exception ex){
			ex.printStackTrace();
			msgLog = new MessageLog(getDebugClassName(), methodName);
			writeLogMessageError(msgLog,ex);
			methodTestStatus.add("Failure");
			methodTestStatus.add(getStackTrace(ex));
		}finally{
			msgLog = new MessageLog(getDebugClassName(), methodName, "Exit");
			writeLogMessageInfo(msgLog);
			getFinalReport().put(getDebugClassName()+"-"+String.valueOf(count), methodTestStatus);
		}
		}


	public void managePaymentMethod() {
		count++;
		methodTestStatus = new ArrayList<String>();
		String methodName = "Manage Payment Method";
		MessageLog msgLog;
		msgLog = new MessageLog(getDebugClassName(), methodName, "Entered");
		writeLogMessageInfo(msgLog);
		methodTestStatus.add(methodName);
		try{
			Actions actions = new Actions(webDriver);
			actions.moveToElement(webDriver.findElement(By.xpath("//*[@id='headerNotShown']/div[2]/div/div/div/div[2]/div/a"))).click(webDriver.findElement(By.xpath("//*[@id='headerNotShown']/div[2]/div/div/div/div[2]/div/div/div[2]/div/div[1]/ul[5]/li[1]/a"))).build().perform();
		/*clickElementByXPath("//*[@id='enrolled-content']/div/div[2]/div[2]/div[1]/div[2]/div[2]/div[2]/span/a");*/
		//webDriver.findElement(By.xpath("//*[@id='enrolled-content']/div/div[2]/div[2]/div[1]/div[2]/div[2]/div[2]/span/a")).click();
		methodTestStatus.add("Success");
		methodTestStatus.add(" ");
		}catch(Exception ex){
			ex.printStackTrace();
			msgLog = new MessageLog(getDebugClassName(), methodName);
			writeLogMessageError(msgLog,ex);
			methodTestStatus.add("Failure");
			methodTestStatus.add(getStackTrace(ex));
		}finally{
			msgLog = new MessageLog(getDebugClassName(), methodName, "Exit");
			writeLogMessageInfo(msgLog);
			getFinalReport().put(getDebugClassName()+"-"+String.valueOf(count), methodTestStatus);
		}
		
	}


	public void viewBill() {
		count++;
		methodTestStatus = new ArrayList<String>();
		String methodName = "View My Bill";
		MessageLog msgLog;
		msgLog = new MessageLog(getDebugClassName(), methodName, "Entered");
		writeLogMessageInfo(msgLog);
		methodTestStatus.add(methodName);
		try{
			Actions actions = new Actions(webDriver);
			actions.moveToElement(webDriver.findElement(By.xpath("//*[@id='headerNotShown']/div[2]/div/div/div/div[2]/div/a"))).click(webDriver.findElement(By.xpath("//*[@id='headerNotShown']/div[2]/div/div/div/div[2]/div/div/div[2]/div/div[1]/ul[1]/li/a"))).build().perform();
			/*webDriver.findElement(By.xpath("//*[@id='enrolled-content']/div/div[2]/div[2]/div[1]/div[2]/span/a/span/div/span/span")).click();
			webDriver.findElement(By.xpath("//*[@id='enrolled-content']/div/div[2]/div[2]/div[1]/div[1]/div[2]/div/div[6]/span/span/a/span/div/span/span")).click();*/
			methodTestStatus.add("Success");
			methodTestStatus.add(" ");
			}catch(Exception ex){
				ex.printStackTrace();
				msgLog = new MessageLog(getDebugClassName(), methodName);
				writeLogMessageError(msgLog,ex);
				methodTestStatus.add("Failure");
				methodTestStatus.add(getStackTrace(ex));
			}finally{
				msgLog = new MessageLog(getDebugClassName(), methodName, "Exit");
				writeLogMessageInfo(msgLog);
				getFinalReport().put(getDebugClassName()+"-"+String.valueOf(count), methodTestStatus);
			}
	}
	
	
	public void oneTimePayment(){
		count++;
		methodTestStatus = new ArrayList<String>();
		String methodName = "One Time Payment";
		MessageLog msgLog;
		msgLog = new MessageLog(getDebugClassName(), methodName, "Entered");
		writeLogMessageInfo(msgLog);
		methodTestStatus.add(methodName);
		try{
			Actions actions = new Actions(webDriver);
			actions.moveToElement(webDriver.findElement(By.xpath("//*[@id='headerNotShown']/div[2]/div/div/div/div[2]/div/a"))).click(webDriver.findElement(By.xpath("//*[@id='headerNotShown']/div[2]/div/div/div/div[2]/div/div/div[2]/div/div[1]/ul[5]/li[3]/a"))).build().perform();
		/*clickElementByXPath("//*[@id='enrolled-content']/div/div[2]/div[2]/div[1]/div[2]/div[2]/div[2]/span/a");*/
		//webDriver.findElement(By.xpath("//*[@id='enrolled-content']/div/div[2]/div[2]/div[1]/div[2]/div[2]/div[2]/span/a")).click();
		methodTestStatus.add("Success");
		methodTestStatus.add(" ");
		}catch(Exception ex){
			ex.printStackTrace();
			msgLog = new MessageLog(getDebugClassName(), methodName);
			writeLogMessageError(msgLog,ex);
			methodTestStatus.add("Failure");
			methodTestStatus.add(getStackTrace(ex));
		}finally{
			msgLog = new MessageLog(getDebugClassName(), methodName, "Exit");
			writeLogMessageInfo(msgLog);
			getFinalReport().put(getDebugClassName()+"-"+String.valueOf(count), methodTestStatus);
		}
		
	}
	
	public void accountActivity(){
		count++;
		methodTestStatus = new ArrayList<String>();
		String methodName = "Account Activity";
		MessageLog msgLog;
		msgLog = new MessageLog(getDebugClassName(), methodName, "Entered");
		writeLogMessageInfo(msgLog);
		methodTestStatus.add(methodName);
		try{
			Actions actions = new Actions(webDriver);
			actions.moveToElement(webDriver.findElement(By.xpath("//*[@id='headerNotShown']/div[2]/div/div/div/div[2]/div/a"))).click(webDriver.findElement(By.xpath("//*[@id='headerNotShown']/div[2]/div/div/div/div[2]/div/div/div[2]/div/div[1]/ul[6]/li/a"))).build().perform();
		/*clickElementByXPath("//*[@id='enrolled-content']/div/div[2]/div[2]/div[1]/div[2]/div[2]/div[2]/span/a");*/
		//webDriver.findElement(By.xpath("//*[@id='enrolled-content']/div/div[2]/div[2]/div[1]/div[2]/div[2]/div[2]/span/a")).click();
		methodTestStatus.add("Success");
		methodTestStatus.add(" ");
		}catch(Exception ex){
			ex.printStackTrace();
			msgLog = new MessageLog(getDebugClassName(), methodName);
			writeLogMessageError(msgLog,ex);
			methodTestStatus.add("Failure");
			methodTestStatus.add(getStackTrace(ex));
		}finally{
			msgLog = new MessageLog(getDebugClassName(), methodName, "Exit");
			writeLogMessageInfo(msgLog);
			getFinalReport().put(getDebugClassName()+"-"+String.valueOf(count), methodTestStatus);
		}
		
	}
	
	public void billingSupport(){
		count++;
		methodTestStatus = new ArrayList<String>();
		String methodName = "Billing Support";
		MessageLog msgLog;
		msgLog = new MessageLog(getDebugClassName(), methodName, "Entered");
		writeLogMessageInfo(msgLog);
		methodTestStatus.add(methodName);
		try{
			Actions actions = new Actions(webDriver);
			actions.moveToElement(webDriver.findElement(By.xpath("//*[@id='headerNotShown']/div[2]/div/div/div/div[2]/div/a"))).click(webDriver.findElement(By.xpath("//*[@id='headerNotShown']/div[2]/div/div/div/div[2]/div/div/div[2]/div/div[1]/ul[7]/li/a"))).build().perform();
		/*clickElementByXPath("//*[@id='enrolled-content']/div/div[2]/div[2]/div[1]/div[2]/div[2]/div[2]/span/a");*/
		//webDriver.findElement(By.xpath("//*[@id='enrolled-content']/div/div[2]/div[2]/div[1]/div[2]/div[2]/div[2]/span/a")).click();
		methodTestStatus.add("Success");
		methodTestStatus.add(" ");
		}catch(Exception ex){
			ex.printStackTrace();
			msgLog = new MessageLog(getDebugClassName(), methodName);
			writeLogMessageError(msgLog,ex);
			methodTestStatus.add("Failure");
			methodTestStatus.add(getStackTrace(ex));
		}finally{
			msgLog = new MessageLog(getDebugClassName(), methodName, "Exit");
			writeLogMessageInfo(msgLog);
			getFinalReport().put(getDebugClassName()+"-"+String.valueOf(count), methodTestStatus);
		}
		
	}
 
 

}
