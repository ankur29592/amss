package com.amss.amssMaster;

import java.util.Map;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.Select;

import com.amss.logs.MessageLog;
import com.amss.utils.PropertyManager;

public class AMSSTest {
	
	static WebDriver webDriver = null;
	static Map<String,String> propertyMap = null;
	static String LaunchUrl = "";
	
	static{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\akshayme\\Desktop\\Root folder\\IMP data\\selenium\\chromedriver.exe");
		/*webDriver = new InternetExplorerDriver();*/
		webDriver = new ChromeDriver();
		PropertyManager.getPropertyManagerInstance();
		propertyMap= PropertyManager.loadFromProperties();
		
		if(("IDALabAlpha").equalsIgnoreCase(propertyMap.get("TestIn"))){
			LaunchUrl = propertyMap.get("IDALabAlphaUrl");
		}if(("IDAProdAlpha").equalsIgnoreCase(propertyMap.get("TestIn"))){
			LaunchUrl = propertyMap.get("IDAProdAlphaUrl");
		}if(("IDAProdBeta").equalsIgnoreCase(propertyMap.get("TestIn"))){
			LaunchUrl = propertyMap.get("IDAProdBetaUrl");
		}
		
		
	}
	
    public void HomePage() throws InterruptedException{
    	
    	Random random = new Random();
    	String amount = String.valueOf(random.nextInt(100)) + ".00";
    	
    	webDriver.get(LaunchUrl);
		webDriver.findElement(By.name("userid")).sendKeys(propertyMap.get("userName"));
		webDriver.findElement(By.name("password")).sendKeys(propertyMap.get("password"));
		webDriver.findElement(By.name("btnSubmit")).click();
		webDriver.findElement(By.name("successOK")).click();
		webDriver.findElement(By.name("ban")).sendKeys(propertyMap.get("accountNo"));
		webDriver.findElement(By.name("continueButton")).click();
		webDriver.findElement(By.name("amountToPay")).sendKeys(amount);
		
		Select payMethod = new Select(webDriver.findElement(By.name("profileIndex")));
		payMethod.selectByValue("-2");
		
		webDriver.findElement(By.name("creditCardForm.creditCard.cardNumber")).sendKeys(propertyMap.get("CCcardNumber"));
		webDriver.findElement(By.name("creditCardForm.creditCard.ccExpDateMonthAndYear")).sendKeys(propertyMap.get("CCexpiryDate"));
		webDriver.findElement(By.name("creditCardForm.creditCard.verificationNumber")).sendKeys(propertyMap.get("securityCode"));
		webDriver.findElement(By.name("creditCardForm.creditCard.custName")).sendKeys(propertyMap.get("cardHolder"));
		webDriver.findElement(By.name("creditCardForm.creditCard.billingZip")).sendKeys(propertyMap.get("zip"));
		webDriver.findElement(By.name("selectProfileButton")).click();
		webDriver.findElement(By.name("submitPaymentsButton")).click(); 
		webDriver.findElement(By.xpath("//*[@id=\"agreeTermsPaymtAmtExceedBal\"]")).click();
		webDriver.findElement(By.xpath("//*[@id=\"agreeTerms\"]")).click();
		webDriver.findElement(By.name("confirmButton")).click();
		webDriver.getPageSource().contains("");
    }
	
       public void viewPaymentActivity() throws InterruptedException{
    	   webDriver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/table[1]/tbody/tr[2]/td/table/tbody/tr[1]/td[2]/table/tbody/tr/td[4]/table[3]/tbody/tr[9]/td[3]/a")).click();
    	   Thread.sleep(1000);
    	   webDriver.findElement(By.xpath("//*[@id=\"table_header\"]/tbody/tr[2]/td[1]/span/a")).click();
      }
       
       
       public void viewDepositActivity() throws InterruptedException{
    	   webDriver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/table[1]/tbody/tr[2]/td/table/tbody/tr[1]/td[2]/table/tbody/tr/td[4]/table[3]/tbody/tr[10]/td[3]/a")).click();
      }
       
       public void viewFailedPayments() throws InterruptedException{
    	   webDriver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/table[1]/tbody/tr[2]/td/table/tbody/tr[1]/td[2]/table/tbody/tr/td[4]/table[3]/tbody/tr[11]/td[3]/a")).click();
    	   Thread.sleep(1000);
    	   webDriver.findElement(By.xpath("//*[@id=\"table_header\"]/tbody/tr[2]/td[1]/span/a")).click();
      }
       
       public void viewEventHistory() throws InterruptedException{
    	   webDriver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/table[1]/tbody/tr[2]/td/table/tbody/tr[1]/td[2]/table/tbody/tr/td[4]/table[3]/tbody/tr[12]/td[3]/a")).click();
      }
       
       
       public void runIDA() throws InterruptedException{
    	   
    	   AMSSTest idaTest = new AMSSTest();
    	   idaTest.HomePage();
    	   Thread.sleep(5000);
    	   idaTest.viewPaymentActivity();
    	   Thread.sleep(5000);
    	   idaTest.viewDepositActivity();
    	   Thread.sleep(5000);
    	   idaTest.viewFailedPayments();
    	   Thread.sleep(5000);
    	   idaTest.viewEventHistory();
       }
       
       public void runAmss() throws InterruptedException{
    	   
    	String methodName = "runAmss()";
   		MessageLog msgLog;
   	
   		System.out.println("ama5r");
    	   
       }
	
}
