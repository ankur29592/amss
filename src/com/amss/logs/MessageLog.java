package com.amss.logs;

public class MessageLog {

	private String className;
	private String methodName;
	private String msgVariable;
	
	public MessageLog(String className, String methodName) {
		super();
		this.className = className;
		this.methodName = methodName;
		this.msgVariable = "";
	}
	
	public MessageLog(String className, String methodName, String msgVariable) {
		super();
		this.className = className;
		this.methodName = methodName;
		this.msgVariable = msgVariable;
	}
	
	

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getMsgVariable() {
		return msgVariable;
	}

	public void setMsgVariable(String msgVariable) {
		this.msgVariable = msgVariable;
	}

	
}
