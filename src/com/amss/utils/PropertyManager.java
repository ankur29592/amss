package com.amss.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertyManager {
	
	private static  PropertyManager instance;
	
	private PropertyManager(){}
	
	public static synchronized PropertyManager getPropertyManagerInstance(){
		
		if(instance == null){
			instance = new PropertyManager();
		}
		return instance;
	}
	
	public static Map<String,String> loadFromProperties(){
		try{
		Properties prop = new Properties();
		Map<String,String> propertyMap = new HashMap<String, String>();
		InputStream input = new FileInputStream("C:\\AMSS_Selenium\\src\\properties\\amss.properties");
		prop.load(input);
		
		Enumeration<?> em = prop.propertyNames();
		while(em.hasMoreElements()){
			String key = (String) em.nextElement();
	        String value = prop.getProperty(key);
			propertyMap.put(key, value);
		}
		return propertyMap;
		}catch(IOException io){
			io.printStackTrace();
			return null;
		}
	}
}
