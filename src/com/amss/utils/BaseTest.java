package com.amss.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amss.logs.MessageLog;

public abstract class BaseTest {


	private static Logger LOGGER = LoggerFactory.getLogger(BaseTest.class);
	protected static WebDriver webDriver = null;
	protected static Map<String,String> propertyMap = null;
	protected static String LaunchUrl = "";
    protected static Map<String,ArrayList<String>> finalReport = new HashMap<String,ArrayList<String>>();
	protected static int count=0;

	static{
		
	/*	DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
		caps.setCapability(
		    InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
		    true);
		caps.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		caps.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
		caps.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);
		caps.setCapability(InternetExplorerDriver.IE_SWITCHES, "-private");*/
		
		System.setProperty("webdriver.chrome.driver", "C:\\selenium\\chromedriver.exe");
		/*webDriver=new InternetExplorerDriver(caps);*/
		/*webDriver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);*/
		webDriver = new ChromeDriver();
		webDriver.manage().window().maximize();
		
		PropertyManager.getPropertyManagerInstance();
		propertyMap= PropertyManager.loadFromProperties();
		
		/*if(("IDALabAlpha").equalsIgnoreCase(propertyMap.get("TestIn"))){
			LaunchUrl = propertyMap.get("IDALabAlphaUrl");
		}if(("IDAProdAlpha").equalsIgnoreCase(propertyMap.get("TestIn"))){
			LaunchUrl = propertyMap.get("IDAProdAlphaUrl");
		}if(("IDAProdBeta").equalsIgnoreCase(propertyMap.get("TestIn"))){
			LaunchUrl = propertyMap.get("IDAProdBetaUrl");
		}*/
		if(("AmssStage").equalsIgnoreCase(propertyMap.get("TestIn"))){
			LaunchUrl = propertyMap.get("AmssStageUrl");
		}
	}


	/**
	 * Return class name for debug/error messages.
	 * @return java.lang.String containing the debug name
	 */
	protected String getDebugClassName() {
		String s = getClass().getName();
		return s.substring(s.lastIndexOf('.') + 1);
	}

	/**
	 * Log info message 
	 * */
	protected void writeLogMessageInfo(MessageLog msgLog){

		String className = msgLog.getClassName();
		String methodName = msgLog.getMethodName();
		String msgVariable = msgLog.getMsgVariable();

		if("".equalsIgnoreCase(msgVariable)){
			LOGGER.info(className+" : "+methodName);
		}else{
			LOGGER.info(className+" : "+methodName+" : "+msgVariable);
		}

	}

	/**
	 * Log debug message 
	 * */
	protected void writeLogMessageDebug(MessageLog msgLog){

		String className = msgLog.getClassName();
		String methodName = msgLog.getMethodName();
		String msgVariable = msgLog.getMsgVariable();

		if("".equalsIgnoreCase(msgVariable)){
			LOGGER.debug(className+" : "+methodName);
		}else{
			LOGGER.debug(className+" : "+methodName+" : "+msgVariable);
		}

	}

	/**
	 * Log error message 
	 * */
	protected void writeLogMessageError(MessageLog msgLog, Exception ex){

		String className = msgLog.getClassName();
		String methodName = msgLog.getMethodName();
		String msgVariable = msgLog.getMsgVariable();

		if("".equalsIgnoreCase(msgVariable)){
			LOGGER.error(className+" : "+methodName, ex);
		}else{
			LOGGER.error(className+" : "+methodName+" : "+msgVariable, ex);
		}

	}
	
	protected String getStackTrace(Exception ex){
		StringWriter errors = new StringWriter();
		ex.printStackTrace(new PrintWriter(errors));
		return errors.toString();
	}

	public void setValueByName(String name,String value){
		webDriver.findElement(By.name(name)).sendKeys(value);
	}

	public void clickElementByName(String name){
		webDriver.findElement(By.name(name)).click();
	}
	
	public void clearElementByName(String name){
		webDriver.findElement(By.name(name)).clear();
	}

	public void clickElementByXPath(String xPath){
		webDriver.findElement(By.xpath(xPath)).click();
	}

	public void setValueById(String id ,String value){
		webDriver.findElement(By.id(id)).sendKeys(value);
	}

	public void setValueByCCSelector(String selector ,String value){
		webDriver.findElement(By.cssSelector(selector)).sendKeys(value);
	}

	public boolean checkElementExist(String xPath ){
		return webDriver.findElements(By.xpath(xPath)).size()>0;
	}

	public static Map<String, ArrayList<String>> getFinalReport() {
		return finalReport;
	}

	public static void setFinalReport(Map<String, ArrayList<String>> finalReport) {
		BaseTest.finalReport = finalReport;
	}

	
}
