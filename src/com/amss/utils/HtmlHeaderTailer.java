package com.amss.utils;

public class HtmlHeaderTailer {
	
	public static String title ="";

	public static String header ="<!DOCTYPE html>"
			+"<html>"
			+"<head>"
			+"<meta charset=\"ISO-8859-1\">"
			+"<title>Report-"+getTitle()+"</title>"
			+"<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">"
			+"<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>"
			+"<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>"
			+"<style>"
			+".container1 {"
			+"margin-top:2%;"
			+"margin-left: 4%;"
			+"margin-right: 4%;"
			+"//border: 1px solid gray;"
			+"}"
			+"</style>"
			+"</head>"
			+"<body class=\"container1\">"
			+"<div class=\"row\">"
			+" <div class=\"col-md-12 text-center\">"
			+" <h3><i><u>Selenium Test Run Report</u></i></h3>"
			+"</div>"
			+"<div class=\"col-md-12\">"
			+" <table class=\"table table-bordered\">"
			+" <thead>"
			+" <tr>"
			+" <th>Instance</th>"
			+"<th>Method</th>"
			+" <th>Status</th>"
			+" <th>Summary</th>"
			+"</tr>"
			+"</thead>"
			+"<tbody>";

	public static String trailer ="</tbody>"
			+"</table>"
			+"</div>"
			+"</div>"
			+"</body>"
			+"</html>";

	public static String getTitle() {
		return title;
	}

	public static void setTitle(String title) {
		HtmlHeaderTailer.title = title;
	}
	
	

}
