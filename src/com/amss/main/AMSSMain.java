package com.amss.main;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.amss.amssMaster.AMSS;
import com.amss.amssMaster.AMSSTest;
import com.amss.logs.MessageLog;
import com.amss.utils.BaseTest;
import com.amss.utils.HtmlHeaderTailer;

public class AMSSMain extends BaseTest{

	
	public static void main(String[] args) throws InterruptedException, IOException {
		long startTime = System.currentTimeMillis();
		AMSS amss = new AMSS();
		amss.runAmss();
		Thread.sleep(4000);
		amss.payBill();
		Thread.sleep(4000);
	  /*  amss.viewBill();
	    Thread.sleep(4000); // will wait for 2 seconds
		amss.managePaymentMethod();
		Thread.sleep(5000);
	    amss.manageAutomaticPayment();
		Thread.sleep(5000);  
		amss.oneTimePayment();
		Thread.sleep(5000);  
		amss.accountActivity();
		Thread.sleep(5000);  
		amss.billingSupport();*/
		BaseTest.getFinalReport();
		String DATE_FORMAT_NOW = "dd-MM-yyyy";
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		String stringDate = sdf.format(date);
		try{
			HtmlHeaderTailer.setTitle(stringDate);
			FileWriter fileWritter = new FileWriter(new File("C:\\Users\\akshayme\\Desktop\\AMSS_Selenium\\Reports\\Report-"+stringDate+".html"));
			fileWritter.write(HtmlHeaderTailer.header);
			Map<String,ArrayList<String>> map = BaseTest.getFinalReport();
			for (Entry<String, ArrayList<String>> entry : map.entrySet()){
				String instance = entry.getKey();
				String[] status = new String[3];
				status = entry.getValue().toArray(status);
				String instanceName[] = instance.split("-");
				fileWritter.write("<tr>");
				fileWritter.write("<td>");
				fileWritter.write(instanceName[0]);
				fileWritter.write("</td>");
				fileWritter.write("<td>");
				fileWritter.write(status[0]);
				fileWritter.write("</td>");
				if(status[1].equalsIgnoreCase("Success")){
				fileWritter.write("<td style=\"background:green\">");
				fileWritter.write(status[1]);
				fileWritter.write("</td>");
				}else{
					fileWritter.write("<td style=\"background:red\">");
					fileWritter.write(status[1]);
					fileWritter.write("</td>");
				}
				fileWritter.write("<td>"); 
				fileWritter.write(status[2]);
				fileWritter.write("</td>");
				fileWritter.write("</tr>");
			}
			
			fileWritter.close();
			long endTime   = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			System.out.println("second : "+totalTime/1000); 
		}catch(Exception ex){
			
		}
		finally{
			
			
		}
	}
	
	
}
