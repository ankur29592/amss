package Library;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
public class Utility {
	
	public static  void takeScreenShots(WebDriver webDriver,String filename) {
		
		try {
			TakesScreenshot ts = (TakesScreenshot) webDriver;
			File Src= ts.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(Src, new File("C:\\Users\\akshayme\\Desktop\\AMSS_Selenium\\ScreenShots\\"+filename+".png"));
		} catch (Exception e) {
			System.out.println("Screen-Shot Action Failed");
			e.printStackTrace();
		} 
	}

}
